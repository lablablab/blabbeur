# Blabbeur
An author-friendly text generation plugin for Unity.

Designed by [Jonathan Lessard](https://twitter.com/GlandeurLessard) and [Quinn Kybartas](https://twitter.com/QKybartas) for the [Chroniqueur](https://lablablab.net/chroniqueur/) emergent narrative project.
Implemented by Quinn Kybartas and maintained by the [LabLabLab](https://lablablab.net/)

[Documentation can be found here](https://www.lablablab.net/?p=701). 

Inspiration drawn from James Ryan's [Expressionist](https://www.jamesryan.world/projects#/expressionist/) and Kate Compton's [Tracery](https://www.brightspiral.com/).

Blabbeur is shared under the MIT Licence.
You can use it for whatever you want, with credit.

If you're using Blabbeur in your project, we'd love to hear about it!
