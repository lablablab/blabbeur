%YAML 1.2
---
# See http://www.sublimetext.com/docs/3/syntax.html
name: Blabbeur Grammar
file_extensions:
  - blab
scope: blabbeur

contexts:
  # The prototype context is prepended to all contexts but those setting
  # meta_include_prototype: false.
  prototype:
    - include: comments
    - include: tags

  main:
    # The main context is the initial starting point of our syntax.
    # Include other contexts from here (or specify them directly).
    - include: keywords
    - include: defs
    - include: strings
    - include: functions
    - include: references
    - include: symbols


  keywords:
    # Keywords are if, else for and while.
    # Note that blackslashes don't need to be escaped within single quoted
    # strings in YAML. When using single quoted strings, only single quotes
    # need to be escaped: this is done by using two single quotes next to each
    # other.
    - match: '\;'
      scope: blabbeur.separator

  defs:
    - match: '(\w+)\:'
      captures:
          1: blabbeur.symbolname

  numbers:
    - match: '\b(-)?[0-9.]+\b'
      scope: blabbeur.numeric

  strings:
    # Strings begin and end with quotes, and use backslashes as an escape
    # character.
    - match: '"'
      scope: blabbeur.string
      push: inside_string

  inside_string:
    - meta_include_prototype: false
    - meta_scope: blabbeur.string
    - match: '\.'
      scope: blabbeur.separator
    - match: '"'
      scope: blabbeur.string
      pop: true

  functions:
    # Functions begin and end with {}
    - match: '{'
      scope: blabbeur.function
      push: inside_function

  inside_function:
    - meta_include_prototype: false
    - meta_scope: blabbeur.function
    - match: '\b(-)?[0-9.]+\b'
      scope: blabbeur.numeric
    - match: '(<|>|>=|<=|==|[||]|&&)'
      scope: blabbeur.separator
    - match: '"'
      scope: blabbeur.string
      push: inside_string
    - match: '}'
      scope: blabbeur.function
      pop: true

  references:
    # References begin and end with <>
    - match: '\<'
      scope: blabbeur.reference
      push: inside_reference

  inside_reference:
    - meta_include_prototype: false
    - meta_scope: blabbeur.reference
    - match: '\.'
      scope: blabbeur.separator
    - match: '\>'
      scope: blabbeur.reference
      pop: true

  symbols:
    # References begin and end with <>
    - match: '\['
      scope: blabbeur.symbol
      push: inside_symbol

  inside_symbol:
    - meta_include_prototype: false
    - meta_scope: blabbeur.symbol
    - match: '\.'
      scope: blabbeur.separator
    - match: '\]'
      scope: blabbeur.symbol
      pop: true

  tags:
    # tags are single words beginning with #
    - match: (#[a-zA-Z0-9~_]+\b)
      scope: blabbeur.tag

  comments:
    # Comments begin with a '//' and finish at the end of the line.
    - match: '//'
      scope: blabbeur.comment
      push:
        # This is an anonymous context push for brevity.
        - meta_scope: blabbeur.commenttext
        - match: $\n?
          pop: true
